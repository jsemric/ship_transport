-- ship_transport.sql : IDS project
-- Jakub Semric xsemri00@stud.fit.vutbr.cz
-- Tomas Strych xstryc05@stud.fit.vutbr.cz
-- VUT FIT Brno 2017 2BIT

DROP TABLE Ship CASCADE CONSTRAINTS;
DROP TABLE Employee CASCADE CONSTRAINTS;
DROP TABLE Harbour CASCADE CONSTRAINTS;
DROP TABLE Sail CASCADE CONSTRAINTS;
DROP TABLE Cargo CASCADE CONSTRAINTS;
DROP TABLE TakePart;
DROP TABLE Distances;
DROP SEQUENCE cargoSeq;
DROP MATERIALIZED VIEW cargoView;

ALTER SESSION SET NLS_DATE_FORMAT = 'DD.MM.YYYY';

CREATE TABLE Harbour (
    id INTEGER,
    city VARCHAR(30),
    state VARCHAR(30),
    capacity INTEGER
);

CREATE TABLE Distances (
    nauticalMiles INTEGER,
    idFrom INTEGER,
    idTo INTEGER
);

CREATE TABLE Ship (
    id INTEGER,
    name VARCHAR(30),
    shipType VARCHAR(10),
    category VARCHAR(20),
    cargoType VARCHAR(20),
    tonnage INTEGER,
    homeDock INTEGER,
    cranes INTEGER,
    pumps INTEGER,
    selfDischarging CHAR(1)
);

CREATE TABLE Employee (
    id INTEGER,
    name VARCHAR(50),
    pid INTEGER NOT NULL,
    department VARCHAR(10),  -- deck, steward , engine
    position VARCHAR(20)     -- https://en.wikipedia.org/wiki/Bulk_carrier
);

CREATE TABLE Sail (
    id INTEGER,
    departure DATE,
    arrival DATE,
    idFrom INTEGER,
    idTo INTEGER,
    idShip INTEGER NOT NULL,
    captain INTEGER NOT NULL
);

CREATE TABLE Cargo (
    id INTEGER,
    type VARCHAR(20),
    tonnage INTEGER,
    idSail INTEGER
);

CREATE TABLE TakePart (
    idSail INTEGER,
    idPerson INTEGER
);

--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
--                  Definition of table constraints
--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

-- Harbour table
ALTER TABLE Harbour ADD CONSTRAINT PK_Harbour PRIMARY KEY (id);

-- Ship table
ALTER TABLE Ship ADD CONSTRAINT PK_Ship PRIMARY KEY (id);
ALTER TABLE Ship ADD CONSTRAINT FK_ShipHomeDock FOREIGN KEY (homeDock)
REFERENCES Harbour ON DELETE CASCADE;

ALTER TABLE Ship ADD CONSTRAINT SelfDischargingValue CHECK
(selfDischarging = 'Y' OR selfDischarging = 'N');

ALTER TABLE Ship ADD CONSTRAINT TankerVsBulker CHECK
((shipType != 'Tanker' OR cranes = 0) AND
(shipType != 'Bulker' OR pumps = 0) AND
(shipType != 'Tanker' OR selfDischarging = 'N'));

-- Employee table
ALTER TABLE Employee ADD CONSTRAINT PK_Emp PRIMARY KEY (id);
ALTER TABLE Employee ADD UNIQUE(pid);
ALTER TABLE Employee ADD CONSTRAINT PersonalID CHECK((MOD(pid, 11) = 0)
and (LENGTH(pid) = 10));

-- Sail table
ALTER TABLE Sail ADD CONSTRAINT PK_Sail PRIMARY KEY (id);
ALTER TABLE Sail ADD CONSTRAINT FK_SailCaptain FOREIGN KEY (captain) REFERENCES
Employee ON DELETE CASCADE;

ALTER TABLE Sail ADD CONSTRAINT FK_SailIdShip FOREIGN KEY (idShip) REFERENCES
Ship ON DELETE CASCADE;

ALTER TABLE Sail ADD CONSTRAINT FK_SailIdFrom FOREIGN KEY (idFrom) REFERENCES
Harbour ON DELETE CASCADE;

ALTER TABLE Sail ADD CONSTRAINT FK_SailIdTo FOREIGN KEY (idTo) REFERENCES
Harbour ON DELETE CASCADE;

-- Cargo table
ALTER TABLE Cargo ADD CONSTRAINT PK_Cargo PRIMARY KEY (id);
ALTER TABLE Cargo ADD CONSTRAINT FK_Cargo FOREIGN KEY (idSail) REFERENCES
Sail ON DELETE CASCADE;

-- Distance table
ALTER TABLE Distances ADD CONSTRAINT MilesGtZero CHECK(nauticalMiles >= 0);
ALTER TABLE Distances ADD CONSTRAINT CheckOneWay CHECK (idTo > idFrom);
ALTER TABLE Distances ADD CONSTRAINT FK_Distance1 FOREIGN KEY (idTo) REFERENCES
Harbour ON DELETE CASCADE;
ALTER TABLE Distances ADD CONSTRAINT FK_Distance2 FOREIGN KEY (idFrom) REFERENCES
Harbour ON DELETE CASCADE;
ALTER TABLE Distances ADD CONSTRAINT PK_Distance PRIMARY KEY (idFrom, idTo);

-- TakePart table
ALTER TABLE TakePart ADD CONSTRAINT FK_TakePartPerson FOREIGN KEY (idPerson)
REFERENCES Employee ON DELETE CASCADE;

ALTER TABLE TakePart ADD CONSTRAINT FK_TakePartShip FOREIGN KEY (idSail)
REFERENCES Sail ON DELETE CASCADE;

ALTER TABLE TakePart ADD CONSTRAINT PK_TP PRIMARY KEY (idSail, idPerson);


--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
--                  Definition of triggers and procedures
--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

CREATE SEQUENCE cargoSeq START WITH 10;
CREATE OR REPLACE TRIGGER cargoPKseq
BEFORE INSERT ON Cargo
FOR EACH ROW
BEGIN
    :new.id := cargoSeq.NEXTVAL;
END;
/

SET SERVEROUTPUT ON;
-- this trigger checks if a new inserted or updated cargo can overload the ship
CREATE OR REPLACE TRIGGER cargoTonnage
BEFORE INSERT OR UPDATE ON Cargo
FOR EACH ROW
DECLARE
    shipTonnage Ship.tonnage%TYPE;
    actualTonnage Cargo.tonnage%TYPE;
BEGIN
    SELECT Sh.tonnage INTO shipTonnage
    FROM Sail S, Ship Sh
    WHERE S.id = :new.idSail AND s.idship = Sh.id;
    
    SELECT SUM(tonnage) INTO actualTonnage
    FROM Cargo WHERE idSail = :new.idSail;
        
    IF (shipTonnage <= actualTonnage + :new.tonnage ) THEN
        DBMS_OUTPUT.put_line('Error, adding cargo will cause overloading of the ship!');
        DBMS_OUTPUT.put_line('Current tonnage: ' || actualTonnage);
        DBMS_OUTPUT.put_line('After adding new cargo: ' ||
        (actualTonnage + :new.tonnage));
        DBMS_OUTPUT.put_line('Ship total tonnage: ' || shipTonnage);
        raise_application_error(-20200, 'Error: cannot insert new cargo');
    END IF;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
        RETURN;
    WHEN OTHERS THEN
        Raise_Application_Error(-20006, 'Fatal error.');
END;
/

-- takes two arguments the ship ID and the number of ports to show
-- show the portsCount nearest ports if the ship is in the port
-- if the ship is currently active then the ships actual sail arrival date and
-- destination city is displayed
CREATE OR REPLACE PROCEDURE shipStatus(shipId IN INTEGER,
                                       portsCount IN INTEGER) IS
    CURSOR allDistances IS SELECT H1.id AS id1, H2.id AS id2, H2.city,
                        MAX(D.nauticalMiles) miles
                        FROM Distances D, Harbour H1, Harbour H2
                        WHERE ((H1.id = D.idFrom AND H2.id = D.idTo) OR
                        (H1.id = D.idTo AND H2.id = D.idFrom))
                        GROUP BY H1.id, H2.id, H2.city
                        ORDER BY miles;
    actualShip Ship%ROWTYPE;
    destinations allDistances%ROWTYPE;
    latestSail Sail%ROWTYPE;
    portCity Harbour.city%TYPE;
    portId Harbour.id%TYPE;
    cnt INTEGER;   -- counter for ports to display
    sailsCount INTEGER;
BEGIN
    -- find a ship, if does not exist exception is handled below
    SELECT * INTO actualShip FROM Ship WHERE id = shipId;

    -- check if ship has ever sailed
    SELECT COUNT(*) INTO sailsCount FROM Sail
    WHERE idShip = shipId AND departure <= CURRENT_DATE;
    cnt := portsCount;
    
    IF sailsCount = 0 THEN
        -- ship has not ever sailed yet, so it is localized at home dock
        portId := actualShip.homeDock;
    ELSE
        -- get latest sail
        SELECT * INTO latestSail
        FROM Sail
        WHERE idShip = shipId AND departure IN (
            SELECT MAX(departure)
            FROM Sail
            WHERE idShip = shipId AND departure <= CURRENT_DATE
        );

        IF latestSail.arrival >= CURRENT_DATE THEN
           SELECT city INTO portCity FROM Harbour WHERE id = latestSail.idTo;
           DBMS_OUTPUT.put_line('Ship is currently sailing.');
           DBMS_OUTPUT.put_line('Arrival: ' || latestSail.arrival);
           DBMS_OUTPUT.put_line('Arrival: ' || portCity);
           RETURN;
        END IF;
        portId := latestSail.idTo;
    END IF;

    -- computation of the distances from the ship actual port
    SELECT city INTO portCity FROM Harbour WHERE id = portId;
    DBMS_OUTPUT.put_line('Distance from '|| portCity ||' to other ports');
    OPEN allDistances;
    LOOP
        FETCH allDistances INTO destinations;
        EXIT WHEN allDistances%NOTFOUND;
        -- check if ship is currently sailing
        IF destinations.id1 = portId THEN
            DBMS_OUTPUT.put_line(destinations.city || ' ' || destinations.miles);
            cnt := cnt - 1;
            IF cnt <= 0 THEN
                RETURN;
            END IF;
        END IF;
    END LOOP;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            DBMS_OUTPUT.put_line('Ship not found or cannot determine a ship' ||
            ' location!');
        WHEN OTHERS THEN
            Raise_Application_Error(-20006, 'Fatal error.');
END;
/

-- this procedure computes and shows employee's sails by specific ship and
-- distance traveled by this ship amongst all his sails in percentage
CREATE OR REPLACE PROCEDURE employeeStats(empId IN INTEGER,
                                          shipId IN INTEGER) IS
    CURSOR sails IS SELECT Sh.id, D.nauticalMiles
        FROM Sail S, Ship Sh, TakePart T, Distances D
        WHERE S.id = T.idsail AND S.idship = Sh.id AND T.idPerson = empId AND
        ((S.idFrom = D.idFrom AND S.idTo = D.idTo) OR
        (S.idFrom = D.idTo AND S.idTo = D.idFrom));

    total INTEGER;
    concr INTEGER;
    milesTotal INTEGER;
    milesConcr INTEGER;
    sailsRow sails%ROWTYPE;
    empName Employee.name%TYPE;
    shipName Ship.name%TYPE;
BEGIN
    total := 0;
    concr := 0;
    milesTotal := 0;
    milesConcr := 0;
    -- get ship and employee names
    SELECT name INTO empName FROM Employee WHERE id = empId;
    SELECT name INTO shipName FROM Ship WHERE id = shipId;

    OPEN sails;
    LOOP
        FETCH sails INTO sailsRow;
        EXIT WHEN sails%NOTFOUND;
        IF (sailsRow.id = shipId) THEN
            concr := concr + 1;
            milesConcr := milesConcr + sailsRow.nauticalMiles;
        END IF;
        total := total + 1;
        milesTotal := milesTotal + sailsRow.nauticalMiles;
    END LOOP;
    DBMS_OUTPUT.put_line(empName || ' ship: ' || shipName ||
    ' - sails: ' || ROUND(100*concr/total, 2) ||
    '% ' || 'Miles: ' || ROUND(100*milesConcr/milesTotal, 2) || '%');
    EXCEPTION
    WHEN ZERO_DIVIDE THEN
        DBMS_OUTPUT.put_line(empName || ' has never sailed with any ship.');
    WHEN NO_DATA_FOUND THEN
        DBMS_OUTPUT.put_line('Ship or Employee not found!');
    WHEN OTHERS THEN
        Raise_Application_Error(-9741, 'Fatal error.');
END;
/

--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
--                      Inserting values to tables
--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

-- inserting harbours
INSERT INTO Harbour VALUES (100, 'Singapore', 'Singapore', 100);
INSERT INTO Harbour VALUES (101, 'Los Angeles', 'USA', 70);
INSERT INTO Harbour VALUES (102, 'Shanghai', 'China', 90);
INSERT INTO Harbour VALUES (103, 'Vancouver', 'Canada', 60);
INSERT INTO Harbour VALUES (104, 'Bangkok', 'Thailand', 30);

-- inserting distances between ports
-- see https://sea-distances.org/ for distances between ports
INSERT INTO Distances VALUES(7669, 100, 101);
INSERT INTO Distances VALUES(2237, 100, 102);
INSERT INTO Distances VALUES(7078, 100, 103);
INSERT INTO Distances VALUES(831, 100, 104);
INSERT INTO Distances VALUES(5708, 101, 102);
INSERT INTO Distances VALUES(1162, 101, 103);
INSERT INTO Distances VALUES(7753, 101, 104);
INSERT INTO Distances VALUES(5110, 102, 103);
INSERT INTO Distances VALUES(2251, 102, 104);
INSERT INTO Distances VALUES(7174, 103, 104);

-- inserting employees
INSERT INTO Employee VALUES (2000,'John Smith',1111111111,'deck',
'Chief Officer');

INSERT INTO Employee VALUES (2001,'Charles Thornthon',2222222222,'deck',
'2nd Officer');

INSERT INTO Employee VALUES (2002,'James Fisher', 3333333333,'deck',
'3rd Officer');

INSERT INTO Employee VALUES (2003,'Joe Benn',4444444444,'deck','Boatswain');

INSERT INTO Employee VALUES (2004,'Brendan Smith',5555555555, 'deck',
'Boatswain');

INSERT INTO Employee VALUES (2005,'John Sharp', 6666666666, 'deck',
'Chief Officer');

INSERT INTO Employee VALUES (2006,'Victor Fasth', 7777777777, 'steward',
'Chief Steward');

INSERT INTO Employee VALUES (2007,'Joe Sutter', 8888888888, 'steward',
'Assistant');

INSERT INTO Employee VALUES (2008,'Ryan Vermete', 9999999999,'steward',
'Chief Cook');

INSERT INTO Employee VALUES (2009,'Patrick Toews',1111111122,'engine',
'1st Assistant');

INSERT INTO Employee VALUES (2010,'Jonathan Kane',1111111133,'engine',
'Chief Engineer');

INSERT INTO Employee VALUES (2011,'Brad Kane',1111111177,'steward',
'Assistant');


-- inserting ships
-- ID, Name, Type, Category, Cargo type, Tonnage, Home dock, cranes, pumps
INSERT INTO Ship VALUES(301,'Santa Maria', 'Tanker', 'Malaccamax', 'oil',
300000, 103, 0, 8, 'N');

INSERT INTO Ship VALUES(302,'Small crane', 'Tanker', 'Panamax', 'chemicals',
75000, 100, 0, 14, 'N');

INSERT INTO Ship VALUES(303, 'Docker', 'Bulker', 'Malaccamax', 'coal', 370000,
103, 4, 0, 'N');

INSERT INTO Ship VALUES(304, 'Nameless', 'Bulker', 'ULCC', 'ore', 500000, 103,
6, 0, 'N');

-- inserting sails
-- ID, Departure, Arrival,  Start, Target, Ship, Captain
-- Singapour - > LA
INSERT INTO Sail VALUES(40000, '3.3.2017', '24.3.2017', 100, 101, 304, 2000);
-- LA -> Vancouver
INSERT INTO Sail VALUES(40001, '26.3.2017', '30.3.2017', 101, 103, 304, 2000);
-- Shanghai -> LA
INSERT INTO Sail VALUES(40002, '23.4.2017', '15.5.2017', 102, 101, 302, 2001);
-- Singapour -> Bangkog
INSERT INTO Sail VALUES(40003, '10.4.2017', '13.4.2017', 100, 104, 301, 2000);

-- inserting cargoes
-- id, type, tonnage, sail_id
INSERT INTO Cargo(type, tonnage, idSail) VALUES('oil', 289200, 40003);

INSERT INTO Cargo(type, tonnage, idSail)
VALUES('hydrogen', 20415, 40002);

INSERT INTO Cargo(type, tonnage, idSail)
VALUES('bromine', 19200, 40002);

INSERT INTO Cargo(type, tonnage, idSail)
VALUES('chlorine', 29200, 40002);

INSERT INTO Cargo(type, tonnage, idSail)
VALUES('iron ore', 498700, 40000);

INSERT INTO Cargo(type, tonnage, idSail)
VALUES('iron ore', 458000, 40001);

INSERT INTO Cargo(type, tonnage, idSail)
VALUES('titanium ore', 40000, 40001);

-- inserting sailors and their sails
INSERT INTO TakePart VALUES(40000, 2000);
INSERT INTO TakePart VALUES(40000, 2001);
INSERT INTO TakePart VALUES(40000, 2002);
INSERT INTO TakePart VALUES(40000, 2003);
INSERT INTO TakePart VALUES(40000, 2004);
INSERT INTO TakePart VALUES(40000, 2005);
INSERT INTO TakePart VALUES(40000, 2006);
INSERT INTO TakePart VALUES(40000, 2007);
INSERT INTO TakePart VALUES(40000, 2008);
INSERT INTO TakePart VALUES(40000, 2009);
INSERT INTO TakePart VALUES(40000, 2010);

INSERT INTO TakePart VALUES(40001, 2000);
INSERT INTO TakePart VALUES(40001, 2001);
INSERT INTO TakePart VALUES(40001, 2002);
INSERT INTO TakePart VALUES(40001, 2003);
INSERT INTO TakePart VALUES(40001, 2004);
INSERT INTO TakePart VALUES(40001, 2005);
INSERT INTO TakePart VALUES(40001, 2007);
INSERT INTO TakePart VALUES(40001, 2008);
INSERT INTO TakePart VALUES(40001, 2009);
INSERT INTO TakePart VALUES(40001, 2010);


INSERT INTO TakePart VALUES(40002, 2000);
INSERT INTO TakePart VALUES(40002, 2001);
INSERT INTO TakePart VALUES(40002, 2002);
INSERT INTO TakePart VALUES(40002, 2003);
INSERT INTO TakePart VALUES(40002, 2004);
INSERT INTO TakePart VALUES(40002, 2005);
INSERT INTO TakePart VALUES(40002, 2007);
INSERT INTO TakePart VALUES(40002, 2008);
INSERT INTO TakePart VALUES(40002, 2010);

INSERT INTO TakePart VALUES(40003, 2001);
INSERT INTO TakePart VALUES(40003, 2002);
INSERT INTO TakePart VALUES(40003, 2003);
INSERT INTO TakePart VALUES(40003, 2004);
INSERT INTO TakePart VALUES(40003, 2006);
INSERT INTO TakePart VALUES(40003, 2007);
INSERT INTO TakePart VALUES(40003, 2009);
INSERT INTO TakePart VALUES(40003, 2010);
INSERT INTO TakePart VALUES(40003, 2011);

COMMIT;

--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
--                Demonstration of triggers and procedues
--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

-- ship does not exists
EXECUTE employeeStats(2005, 431134);
-- employee does not exists
EXECUTE employeeStats(2004335, 303);
-- employee has sailed with this ship
EXECUTE employeeStats(2005, 304);
-- employee has not sailed with this ship
EXECUTE employeeStats(2005, 303);

-- non existing ship => error
EXECUTE shipStatus(307, 3);
-- ship currently sailing => no distances
EXECUTE shipStatus(302, 3);
-- ship not sailing => distances from Bangkog
EXECUTE shipStatus(301, 3);
-- ship in home dock => distances from Vancouver
EXECUTE shipStatus(303, 4);

-- inserting an cargo which overload the ship
--INSERT INTO Cargo(type, tonnage, idSail) VALUES('iron ore', 30700, 40000);

--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
--  Granting access rigths, creating index, explain plan and materalized view
--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

GRANT EXECUTE ON employeeStats TO xstryc05;
GRANT EXECUTE ON shipStatus TO xstryc05;
GRANT ALL ON Sail TO xstryc05;
GRANT ALL ON Ship TO xstryc05;
GRANT ALL ON Employee TO xstryc05;
GRANT ALL ON Cargo TO xstryc05;
GRANT ALL ON Harbour TO xstryc05;
GRANT ALL ON TakePart TO xstryc05;
GRANT ALL ON Distances TO xstryc05;

-- materialized view
CREATE MATERIALIZED VIEW LOG ON Cargo WITH PRIMARY KEY, ROWID(type)
INCLUDING NEW VALUES;
CREATE MATERIALIZED VIEW cargoView CACHE BUILD IMMEDIATE REFRESH FAST ON
COMMIT ENABLE QUERY REWRITE AS
SELECT type, COUNT(*) AS shipment_count
FROM Cargo
GROUP BY type;

GRANT ALL ON cargoView TO xstryc05;

SELECT * FROM cargoView;
INSERT INTO Cargo(type, tonnage, idSail) VALUES('chlorine', 10000, null);
COMMIT;
SELECT * FROM cargoView;

-- explain plan with index and without comparison

--DROP INDEX idx4explain;

EXPLAIN PLAN FOR
SELECT E.id, E.name, COUNT(*) AS sails
FROM Employee E LEFT OUTER JOIN TakePart T ON T.idPerson = E.id
GROUP BY E.id, E.name
ORDER BY sails DESC, E.name;
SELECT * FROM TABLE(DBMS_XPLAN.display);

-- creating an index
CREATE INDEX idx4explain ON Employee (id, name);

EXPLAIN PLAN FOR
SELECT E.id, E.name, COUNT(*) AS sails
FROM Employee E LEFT OUTER JOIN TakePart T ON T.idPerson = E.id
GROUP BY E.id, E.name
ORDER BY sails DESC, E.name;
SELECT * FROM TABLE(DBMS_XPLAN.display);

COMMIT;